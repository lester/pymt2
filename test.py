import pytest
import numpy as np
from mt2 import mt2, mt2_array
import phasespace
from phasespace import GenParticle

def test_example():
    """
    Example from the comments in the header library
    """
    mVisA = 10
    pxA = 20
    pyA = 30

    mVisB = 10
    pxB = -20
    pyB = -30

    pxMiss = -5
    pyMiss = -5

    chiA = 4
    chiB = 7

    desiredPrecisionOnMt2 = 0

    assert mt2(
        mVisA, pxA, pyA,
        mVisB, pxB, pyB,
        pxMiss, pyMiss,
        chiA, chiB,
        desiredPrecisionOnMt2
    ) == 17.0


@pytest.fixture
def example_arrays():
    prod_blob = GenParticle("", 2000)
    susy1 = GenParticle("SUSY1", 500)
    susy2 = GenParticle("SUSY2", 500)
    neutralino1 = GenParticle("Neutralino1", 100)
    neutralino2 = GenParticle("Neutralino2", 100)
    vis1 = GenParticle("Lepton1", 0)
    vis2 = GenParticle("Lepton2", 0)

    susy1 = susy1.set_children(neutralino1, vis1)
    susy2 = susy2.set_children(neutralino2, vis2)
    prod_blob = prod_blob.set_children(susy1, susy2)
    weights, particles = prod_blob.generate(n_events=10000)

    pxMiss = (particles["Neutralino1"][:,0] + particles["Neutralino2"][:,0]).numpy()
    pyMiss = (particles["Neutralino1"][:,1] + particles["Neutralino2"][:,1]).numpy()
    pxVis1 = particles["Lepton1"][:,0].numpy()
    pyVis1 = particles["Lepton1"][:,1].numpy()
    pxVis2 = particles["Lepton2"][:,0].numpy()
    pyVis2 = particles["Lepton2"][:,1].numpy()

    return pxMiss, pyMiss, pxVis1, pyVis1, pxVis2, pyVis2


def test_consistency(example_arrays):

    pxMiss, pyMiss, pxVis1, pyVis1, pxVis2, pyVis2 = example_arrays

    @np.vectorize
    def susy_mt_v(pxMiss, pyMiss, pxVis1, pyVis1, pxVis2, pyVis2):
        return mt2(0, pxVis1, pyVis1, 0, pxVis2, pyVis2, pxMiss, pyMiss, 100, 100)

    assert (
        susy_mt_v(pxMiss, pyMiss, pxVis1, pyVis1, pxVis2, pyVis2)
        == mt2_array(0, pxVis1, pyVis1, 0, pxVis2, pyVis2, pxMiss, pyMiss, 100, 100)
    ).all()


def test_min_max(example_arrays):
    pxMiss, pyMiss, pxVis1, pyVis1, pxVis2, pyVis2 = example_arrays
    array = mt2_array(0, pxVis1, pyVis1, 0, pxVis2, pyVis2, pxMiss, pyMiss, 100, 100)
    assert array.min() > 100
    assert array.max() < 500

from setuptools import setup, find_packages, Extension

setup(
    name="mt2",
    version="0.5",
    packages=["mt2"],
    python_requires=">=3",
    ext_modules=[Extension("mt2.libMT2", sources=["src/c_api.cpp"], language="c++")],
    author="Nikolai Hartmann",
    author_email="nihartma@cern.ch",
    description="Python interface for MT2 (https://www.hep.phy.cam.ac.uk/~lester/mt2/)",
    url="https://gitlab.cern.ch/nihartma/pymt2",
)

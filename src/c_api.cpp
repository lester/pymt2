#include <cstddef>
#include "lester_mt2_bisect.h"

extern "C" {

  double mt2(double mVis1, double pxVis1, double pyVis1,
             double mVis2, double pxVis2, double pyVis2,
             double pxMiss, double pyMiss,
             double mInvis1, double mInvis2,
             double desiredPrecisionOnMT2,
             bool useDeciSectionsInitially) {
    asymm_mt2_lester_bisect::disableCopyrightMessage();
    return asymm_mt2_lester_bisect::get_mT2(mVis1, pxVis1, pyVis1,
                                            mVis2, pxVis2, pyVis2,
                                            pxMiss, pyMiss,
                                            mInvis1, mInvis2,
                                            desiredPrecisionOnMT2,
                                            useDeciSectionsInitially);
  }

  void fill_mt2_array(double mVis1, double *pxVis1, double *pyVis1,
                      double mVis2, double *pxVis2, double *pyVis2,
                      double *pxMiss, double *pyMiss,
                      double *output,
                      size_t n_entries,
                      double mInvis1, double mInvis2,
                      double desiredPrecisionOnMT2,
                      bool useDeciSectionsInitially) {
    asymm_mt2_lester_bisect::disableCopyrightMessage();
    for (int i=0; i < n_entries; ++i) {
      output[i] = asymm_mt2_lester_bisect::get_mT2(mVis1, pxVis1[i], pyVis1[i],
                                                   mVis2, pxVis2[i], pyVis2[i],
                                                   pxMiss[i], pyMiss[i],
                                                   mInvis1, mInvis2,
                                                   desiredPrecisionOnMT2,
                                                   useDeciSectionsInitially);
    }
  }

}
